import itertools
import collections
import multiprocessing.pool
import json
import gzip
from collections import defaultdict
import subprocess
import datetime
from orderedset import OrderedSet
from multiprocessing.dummy import Pool as ThreadPool

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

def worker(arg):
    errorList = list()
    url = arg
    try:
        rdatalist = subprocess.check_output(['dig','+noall', url, 'A','+answer', url, 'AAAA', '+answer'])
        rdatalist = rdatalist.replace('\t', ' ')
        rdatalist = rdatalist.splitlines()
        response_dict = collections.defaultdict(lambda: defaultdict(list))
        cnameset = OrderedSet()
        if(len(rdatalist) > 0):
            for record in rdatalist:
                reccontent = record.split()
                qname = reccontent[3]
                value = reccontent[4]
                if(qname == 'A'):
                    cmd = 'nping'
                    try:
                        rtt = subprocess.check_output([cmd,'--tcp-connect','-c', '1', '-p', '443','--flags', 'syn' , value]
                                                      ,stderr=subprocess.STDOUT).splitlines()[5].rpartition('|')[-1].split(' ')[3][:-2]
                        response_dict[url][qname].append({'IP': value,'rtt':rtt})
                    except subprocess.CalledProcessError as e:
                        log_error("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
                if (qname == 'AAAA'):
                    cmd = 'nping'
                    try:
                        rtt = subprocess.check_output([cmd,'-6','--tcp-connect','-c', '1', '-p', '443','--flags', 'syn' , value]
                                                      ,stderr=subprocess.STDOUT).splitlines()[5].rpartition('|')[-1].split(' ')[3][:-2]
                        response_dict[url][qname].append({'IP6': value, 'rtt': rtt})
                    except subprocess.CalledProcessError as e:
                        log_error(
                            "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
                if (qname == 'CNAME'):
                    cnameset.add(value)

        response_dict[url]['CNAME'].extend(list(cnameset))
        return response_dict
    except AttributeError as e:
        log_error("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
        return []
    except subprocess.CalledProcessError as e:
        log_error("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
        return  []
    except e:
        log_error("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
        return []


def log_error(msg):
    f = open('errors.txt', 'a+')
    f.write(msg)

def save_to_file(dictionary, filename):
    jsondata = json.dumps(dictionary, cls=SetEncoder)
    with gzip.open(filename+'.json.gz', 'wb') as f:
        f.write(jsondata)

def load_from_file(filename):
    with gzip.open(filename, 'rb') as f:
        file_content = f.read()
    read_dictionary = json.loads(file_content)
    return  read_dictionary

def init(queue):
    global idx
    idx = queue.get()

#
def resolve_dns(url_list):
    """Given a list of hosts, return dict that maps qname to
    returned rdata records.
    """
    #response_dict = collections.defaultdict(defaultdic^t)
    # create pool for querys but cap max number of threads
    pool = multiprocessing.pool.ThreadPool(processes=60)
    #print 'url_list_len:'+ str(len(url_list))
    results = pool.map(worker, url_list)
    pool.close()
    pool.join()
    #save_to_file(errorList,'error_'+str(cnt)+'.json.gz')
    return  results # response_dict

print "start: "+ str(datetime.datetime.now())
print "start reading: " + str(datetime.datetime.now())
with gzip.open('majestics_cdn_discovery.json.gz', 'rb') as f:
    file_content = f.read()

url_list = json.loads(file_content)
print "end reading: " + str(datetime.datetime.now())

#url_list = ['www2.shutterfly.com','www.nationalgeographic.com']
n = 50000
for i in xrange(0, len(url_list), n):
    results = resolve_dns(url_list[i:i + n])
    # save last part
    print "done (chunk "+str(i+n) + "): " + str(datetime.datetime.now())
    filename = 'result_chunk_'+str(i+n)
    # write to file
    save_to_file(results, filename)

