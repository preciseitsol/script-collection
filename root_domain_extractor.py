import tldextract
import mysql.connector

cnx = mysql.connector.connect(user='root', password='H1gh5ecure,15', host='127.0.0.1', database='cdndetection')
cursor = cnx.cursor()

def getdomainsfromdb():
    query = ("SELECT distinct domain from domains")
    cursor.execute(query)
    result = cursor.fetchall()
    return [row[0] for row in result]

add_rootdomain = ('INSERT INTO rootdomain (domain) VALUES (%s);')


domainlist = getdomainsfromdb()
print 'fetching done'
for domain in domainlist:
    result = tldextract.extract(domain);
    if result.suffix == '':
        continue

    if '.'.join(result[1:3]) == '.blog':
        print result
    #try:
    #    cursor.execute(add_rootdomain, data_rootdomain)
    #except mysql.connector.errors.IntegrityError:
    #    continue

#cnx.commit()
cursor.close()
cnx.close()