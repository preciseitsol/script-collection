import dns
from dns import resolver
import itertools
import collections
import multiprocessing.pool
import tldextract
import json
import gzip
from collections import defaultdict
import subprocess
import datetime


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

def worker(arg):
    """query dns for (hostname, qname) and return (qname, [rdata,...])"""
    try:
        url, qname = arg
        rdatalist = [rdata for rdata in resolver.query(url, qname)]
        return url, qname, rdatalist
    except dns.exception.DNSException, e:
        return url, qname, []

def resolve_dns(url_list):
    """Given a list of hosts, return dict that maps qname to
    returned rdata records.
    """
    cnt = 0
    errorList = list()
    response_dict = collections.defaultdict(defaultdict)
    # create pool for querys but cap max number of threads
    pool = multiprocessing.pool.ThreadPool(processes=60)
    # run for all combinations of hosts and qnames1
    for url, qname, rdatalist in pool.imap(
            worker,
            itertools.product(url_list, ('CNAME','A')),
            chunksize=1):


        cnt += 1
        if ((cnt % 1000) == 0):
            print "already read: " + str(cnt) + " now: "+ url + " at " + str(datetime.datetime.now())

        measurement = list();

        if(len(rdatalist) > 0):
            for record in rdatalist:
                if(qname == 'A'):
                    cmd = 'ping'
                    try:
                        rtt = subprocess.check_output([cmd, '-c', '3', str(record)]).splitlines()[-1].rpartition('=')[-1].split('/')[1]
                        measurement.append({'IP': record.to_text(),'rtt':rtt})
                    except subprocess.CalledProcessError as e:
                        errorList.append("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
                if (qname == 'CNAME'):
                    measurement.append(record.to_text())
                    #record = rdatalist[0].to_text()

            response_dict[url][qname] = measurement


    pool.close()
    print errorList
    return response_dict

print "start: "+ str(datetime.datetime.now())
print "start reading: " + str(datetime.datetime.now())
with gzip.open('majestics_cdn_discovery.json.gz', 'rb') as f:
    file_content = f.read()

url_list = json.loads(file_content)
print "end reading: " + str(datetime.datetime.now())

#url_list = ['www.lemonde.fr','www.orf.at']
result = resolve_dns(url_list)
jsondata = json.dumps(result,cls=SetEncoder)

with gzip.open('results.json.gz', 'wb') as f:
    f.write(jsondata)


#[line.rpartition('=')[-1] for line in subprocess.check_output(['ping', '-c', '3', 'localhost']).splitlines()[1:-4]]