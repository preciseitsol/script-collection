import json
import gzip
import os
import mysql.connector
from collections import defaultdict
import glob
import ipaddress
import socket, struct

glob.glob('*.gz')

cnx = mysql.connector.connect(user='root', password='H1gh5ecure,15', host='127.0.0.1', database='cdndetection')
cursor = cnx.cursor(dictionary=True)

sourcedir = '/home/gerald/Dropbox/Uni/Master/MasterArbeit/Data/RAW'

domaindict = defaultdict()
ipdict = defaultdict()

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

def load_from_file(filename):
    with gzip.open(filename, 'rb') as f:
        file_content = f.read()
    jcontent = json.loads(file_content)
    return  jcontent


def readallresults(vpname, vpid):
    directory = sourcedir + '/' +vpname
    files = os.listdir(directory) #sorted(os.listdir(directory), key=lambda fn:os.path.getctime(os.path.join(directory,fn)))

    results = defaultdict(dict)

    add_ip = ('INSERT INTO ips (ip, version) VALUES (INET6_ATON(%s),%s);')
    add_distance = ('INSERT INTO vpipdistance (vp,ip, distance) VALUES (%s,%s,%s);')
    add_cname = ('INSERT INTO vpipdistance (vp,ip, distance) VALUES (%s,%s,%s);')
    add_ipdomain = ('INSERT INTO domainip (domain,ip, vp) VALUES (%s,%s,%s);')
    data_ip = ()
    data_distance = ()
    data_ipdomain = ()
    ipcnt = 0
    for filename in files:

        if filename.endswith('.gz') == False:
            continue
        junk = load_from_file(directory+'/'+filename)
        #print 'file: ' + directory+'/'+filename
        for elem in junk:
            try:
                domain = next(iter(elem))
                domain_no = domaindict.get(domain)
                entries = elem.values()[0]
                prev = domain
                for key,entry  in entries.iteritems():
                    if(len(entry) == 0):
                        continue
                    cnt = 0
                    for value in entry:
                        cnt += 1
                        if(key == 'A'):
                            ipcnt+=1
                            ip_no = 0
                            ip = value.get('IP')
                            if ip in ipdict:
                                ip_no = ipdict.get(ip)
                            else:
                                data_ip = (ip,4)
                                cursor.execute(add_ip, data_ip)
                                ip_no = cursor.lastrowid
                                ipdict[ip] = ip_no
                            rtt = value.get('rtt')
                            if rtt == 'N':
                                rtt = -1
                            if str(rtt).startswith("("):
                                rtt = rtt[1:]
                            data_distance = (vpid,ip_no,rtt)
                            try:
                                cursor.execute(add_distance,data_distance)
                            except mysql.connector.errors.IntegrityError:
                                pass

                            try:
                                data_ipdomain = (domain_no, ip_no, vpid)
                                cursor.execute(add_ipdomain,data_ipdomain)
                            except mysql.connector.errors.IntegrityError:
                                pass

                        if(key == 'AAAA'):
                            ipcnt += 1
                            ip_no = 0
                            ip = value.get('IP6')
                            if ip in ipdict:
                                ip_no = ipdict.get(ip)
                            else:
                                data_ip = (ip,6)
                                cursor.execute(add_ip, data_ip)
                                ip_no = cursor.lastrowid
                                ipdict[ip] = ip_no
                            rtt = value.get('rtt')
                            if rtt == 'N':
                                rtt = -1
                            if str(rtt).startswith("("):
                                rtt = rtt[1:]
                            data_distance = (vpid,ip_no,rtt)
                            try:
                                cursor.execute(add_distance,data_distance)
                            except mysql.connector.errors.IntegrityError:
                                pass

                            try:
                                data_ipdomain = (domain_no, ip_no, vpid)
                                cursor.execute(add_ipdomain,data_ipdomain)
                            except mysql.connector.errors.IntegrityError:
                                pass

                        if(key == 'CNAME'):
                            cname = value[:-1]
                            cname_no = 0
                            if cname in domaindict:
                                cname_no = domaindict.get(cname)
                            else:
                                try:
                                    add_domain = ('INSERT INTO domains (domain) VALUES (%s);')
                                    data_domain = (cname,)
                                    cursor.execute(add_domain, data_domain)
                                    cname_no = cursor.lastrowid
                                    domaindict[cname] = cname_no
                                except:
                                    continue
                            try:
                                add_cname = ('INSERT INTO domaincname (domain, cname, vp) VALUES (%s,%s,%s);')
                                data_cname = (domaindict.get(prev),cname_no,vpid)
                                cursor.execute(add_cname, data_cname)
                            except mysql.connector.errors.IntegrityError:
                                continue
                            finally:
                                prev = cname
            except StopIteration:
                continue

    cnx.commit()
    print ipcnt
    return  results

def writedomainstotb(vpname):
    directory = sourcedir + '/' +vpname
    files = sorted(os.listdir(directory), key=lambda fn:os.path.getctime(os.path.join(directory,fn)))
    results = defaultdict(dict)
    for filename in files:
        if filename.endswith('.gz') == False:
            continue
        print 'found: '+filename
        junk = load_from_file(directory+'/'+filename)
        for elem in junk:

            try:
                domain = next(iter(elem))
            except StopIteration:
                continue
            if domain in domaindict:

                continue
            add_domain = ('INSERT INTO domains (domain) VALUES (%s);')
            data_domain = (domain,)
            cursor.execute(add_domain,data_domain)
            domain_no = cursor.lastrowid
            domaindict[domain] = domain_no
            #print next(iter(elem))2

    cnx.commit()
    return  results

def getdomainsfromdb():
    query = ("SELECT * from domains")
    cursor.execute(query)
    listresult = cursor.fetchall()
    newdict ={}
    for item in listresult:
        newdict[item.get('domain')]=item.get('iddomains')
    return newdict

def getipsformdoain():
    query = ("SELECT idips, INET6_NTOA(ip) as ip, version from ips")
    cursor.execute(query)
    listresult = cursor.fetchall()
    newdict ={}
    for item in listresult:
        ip = item.get('ip')
        if item.get('version') == 6:
            packedIP = socket.inet_pton(socket.AF_INET6, ip)
            ip = socket.inet_ntop(socket.AF_INET6, packedIP)
        newdict[ip]=item.get('idips')

    return newdict

def readallresults2(vpname, vpid):
    directory = sourcedir + '/' +vpname
    files = os.listdir(directory) #sorted(os.listdir(directory), key=lambda fn:os.path.getctime(os.path.join(directory,fn)))

    for filename in files:
        if filename.endswith('.gz') == False:
            continue
        junk = load_from_file(directory+'/'+filename)
        #print 'file: ' + directory+'/'+filename
        for elem in junk:
            try:
                domain = next(iter(elem))
                domain_no = domaindict.get(domain)
                entries = elem.values()[0]
                prev = domain
                for key, entry in entries.iteritems():
                    if (len(entry) == 0):
                        continue
                    for value in entry:
                        if (key == 'A' or key == 'AAAA'):
                            ip = value.get('IP')
                            version = 4
                            if key == 'AAAA':
                                ip = value.get('IP6')
                                version = 6
                            add_ip = ('INSERT INTO ips2 (ip, version) VALUES (INET6_ATON(%s),%s);')
                            data_ip = (ip, version)
                            cursor.execute(add_ip, data_ip)
                            ip_no = cursor.lastrowid
                            ipdict[ip] = ip_no
            except StopIteration:
                continue
    cnx.commit()




#pip = ipaddress.IPv6Address(u'2a01:488:66:1000:5bfa:728c:0:1').packed
#print str(pip)

vps = ((3,'london'),(4,'mumbai'),(5,'oregon'),(6,'sydney'),(7,'saopaulo'))

domaindict = getdomainsfromdb()
print 'read domains done'
ipdict = getipsformdoain();
print 'read IPs done'
for vp in vps:
    vpid = vp[0]
    vpname = vp[1]
    results = readallresults(vpname,vpid)
#results = readallresults2(vp_ff_dir,vp_ff_id)
cursor.close()
cnx.close()
#print results

