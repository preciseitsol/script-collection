import pandas
import tldextract
import json
import gzip
from collections import defaultdict

def prependdomainname(domainname):
    subdomains = ('www','www2','www3')
    retlist = list()
    for subdomain in subdomains:
        retlist.append(subdomain+"."+domain)
    return retlist

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

io = pandas.read_csv('/home/gerald/Downloads/majestic_million_short.csv',sep=",",usecols=(2,))

domains = io["Domain"].tolist()

domainList = list()

for domain in domains:
    root = tldextract.extract(domain)
    rootdomain = domain
    if(root.domain != ""):
        rootdomain = root.domain+"."+root.suffix
        domainList.append(domain)

    if(rootdomain == domain):
        domainList.extend(prependdomainname(rootdomain))
    #    print domainDict[rootdomain]
    #if(rootdomain != domain):

# writing
jsondata = json.dumps(domainList,cls=SetEncoder)

with gzip.open('majestics_cdn_discovery_short.json.gz', 'wb') as f:
    f.write(jsondata)
